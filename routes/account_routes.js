/**
 * Created by student on 11/27/17.
 */
var express = require('express');
var router = express.Router();

var account_dal = require('../model/account_dal');


// View All companys
router.get('/all', function(req, res) {
    account_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('account/accountViewAll', { 'result':result });
        }
    });

});
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.email == null) {
        res.send('Street Name must be provided.');
    }
    else if(req.query.zip_code == null) {
        res.send('Zip code must be entered');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        address_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/address/all');
            }
        });
    }
});


router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    account_dal.getAll(function(err,result) {
        if (err) {
            res.send(err);
        }
        else {
            res.render('account/accountAdd', {'address': result});
        }
    });
});
module.exports = router;