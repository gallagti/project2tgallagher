var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM address;';

    connection.query(query, function(err, result) {
        callback(err, result);



    });

};

exports.insert = function(params, callback) {

    // Insert the address
    var query = 'INSERT INTO address (street, zip_code) VALUES ?;';
    var queryData = [];
    queryData.push([params.street, params.zip_code]);
    connection.query(query, [queryData], function(err, result){
        callback(err, result)
        });
};


exports.update = function(params, callback) {
    var query = 'UPDATE address SET street = ?, zip_code = ? WHERE address_id = ?;';
    // console.log(params.street);
    // console.log(params.zip_code);
    // console.log(params.address_id);
    var queryData = [params.street, params.zip_code, params.address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);

    });
};


exports.edit = function(address_id, callback) {
    var query = 'CALL address_getinfo(?)';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};



exports.delete = function(address_id, callback) {
    var query = 'DELETE FROM address WHERE address_id = ?;';
    var queryData = [address_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.getById = function(address_id, callback) {
    var query = 'SELECT street, zip_code FROM address ' +
        'WHERE address.address_id = ?;';
    var queryData = [address_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {

        callback(err, result);
    });
};

// this function works, just don't want random deletions
// exports.delete = function(address_id, callback) {
//     var query = 'DELETE FROM address WHERE address_id = ?';
//     var queryData = [address_id];
//
//     connection.query(query, queryData, function(err, result) {
//         callback(err, result);
//     });
//});



// exports.getById = function(address_id, callback) {
//     var query = 'SELECT c.*, a.street, a.zip_code FROM company c ' +
//         'LEFT JOIN company_address ca on ca.company_id = c.company_id ' +
//         'LEFT JOIN address a on a.address_id = ca.address_id ' +
//         'WHERE c.company_id = ?';
//     var queryData = [company_id];
//     console.log(query);
//
//     connection.query(query, queryData, function(err, result) {
//
//         callback(err, result);
//     });
// };